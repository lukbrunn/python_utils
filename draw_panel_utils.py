#!/usr/bin/python
'''
File Name : draw_panel_utils.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 27-04-2016
Modified: Tue 09 Aug 2016 02:07:36 PM CEST
Purpose: functions to simplify panel plotting, e.g. four seasons into one figure


'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mc
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid, maskoceans
from pylab import *

def draw_panel_2x2_h(data,lats,lons,plotname='outfig.pdf',title=None,subtitles=None, figsize=(12,8),method='contourf', vmin=None, vmax=None, colors='YlOrRd',levels=15,masko=None, region=None):

    fig=plt.figure(figsize=figsize)
    if title:
        plt.suptitle(title,fontsize=15)
    gs = gridspec.GridSpec(3,2 ,width_ratios=[1,1],height_ratios=[1,1,0.3],hspace=0.1,wspace=0.1 ,top=0.95,right=0.95,left=0.05,bottom=0.05)

    min_lat = np.amin(lats)
    max_lat = np.amax(lats)
    max_lon = np.amax(lons)
    min_lon = np.amin(lons)
    pltno=0
    for row in range(0,2):
        for col in range(0,2):
            ax = plt.subplot(gs[row, col])
            if (pltno%2 == 0):
                dpl = 1
            else:
                dpl = 0
            if (row == 1):
                dml = 1
            else:
                dml = 0
            if region:
                if region=='ARC':
                        #for arctic region draw polar stereographic projection
                        m = Basemap(projection='npstere',boundinglat=70,lon_0=270,resolution='c')
                else:
                        m = Basemap(projection='cyl',llcrnrlat=min_lat,urcrnrlat=max_lat, llcrnrlon=min_lon,urcrnrlon=max_lon,resolution='c')
                m.drawcoastlines()
                m.drawparallels(np.arange(-90.,91.,10.), labels=[dpl,0,0,0])
                m.drawmeridians(np.arange(-180.,181.,20.), labels=[0,0,0,dml])
            else:
                m = Basemap(projection='cyl',llcrnrlat=min_lat,urcrnrlat=max_lat, llcrnrlon=-180,urcrnrlon=180,resolution='c')
                m.drawcoastlines()
                m.drawparallels(np.arange(-90.,91.,30.), labels=[dpl,0,0,0])
                m.drawmeridians(np.arange(-180.,181.,60.), labels=[0,0,0,dml])
	    if region is None:
            	if (min_lon >= 0) and (max_lon > 180):
                	if (pltno == 0):
                    		lons_orig = lons[:]
                	data_plt,lons =  shiftgrid(180., data[pltno,:,:], lons_orig,start=False)
            else:
                data_plt = data[pltno,:,:]
            data_plt, lonsnew = addcyclic(data_plt, lons)
            lons2d, lats2d = np.meshgrid(lonsnew, lats)
            x, y = m(lons2d, lats2d)
            if masko:
                data_plt = maskoceans(lons2d, lats2d, data_plt, resolution = 'h', grid = 1.25, inlands=True)
            cmap=plt.get_cmap(colors)
    
            if (type(levels) != int):
                diff = (levels[0]-levels[1])-(levels[1]-levels[2])
            else:
                diff=0
            if isinstance(levels, int) | (diff==0):
                cs = m.contourf(x,y,data_plt,levels,cmap=cmap,extend='both')
            else:
                norm=mc.BoundaryNorm(levels, cmap.N)
                cs = ax.contourf(x,y,data_plt,levels,cmap=cmap,norm=norm,extend='both')

            if subtitles:
                plt.title(subtitles[pltno],size=15,ha='left',x=0)

            pltno=pltno+1

    cbar_ax = fig.add_axes([0.15, 0.15, 0.7, 0.03])
    cbar = fig.colorbar(cs,cax=cbar_ax, orientation = 'horizontal')
    cbar.ax.tick_params(labelsize=15)

    #gs.tight_layout(fig)
    fig1=plt.gcf()
    #plt.show()
    #plt.draw()
    fig1.savefig(plotname,dpi=fig.dpi)
            
