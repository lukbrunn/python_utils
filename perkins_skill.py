#!/usr/bin/python
'''
File Name : perkins_skill.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 16-03-2016
Modified: Thu 07 Jun 2018 05:19:52 PM CEST
Purpose: calculate Perkins skill score (Perkins et al. 2007, J.Clim)


'''
import numpy as np

#needs two distributions to compare
#calculated density with numpy.histogram
#hist, bin_edges = np.histogram(data,bins=np.arange(np.min(data),np.max(data),0.5),density=True))
#density = hist*np.diff(bin_edges)
def perkins_skill(pdf1, pdf2):
    mins = np.minimum(pdf1,pdf2)
    ss = np.nansum(mins)
    return ss
